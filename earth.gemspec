# -*- encoding: utf-8 -*-
$:.unshift File.expand_path("../lib", __FILE__)
require "earth"

Gem::Specification.new do |s|
  s.name          = "earth"
  s.version       = Earth::VERSION
  s.authors       = [ "Stephane D'Alu" ]
  s.email         = [ "stephane.dalu@gmail.com" ]
  s.homepage      = "http://github.com/sdalu/ruby-earth"
  s.summary       = "Calculate distance on Earth"
  #s.description   = "Calculate distance on Earth"

  s.add_development_dependency "yard"
  s.add_development_dependency "rake"

  s.has_rdoc      = 'yard'

  s.license       = 'MIT'
  

  s.files         = %w[ LICENSE Gemfile earth.gemspec ] 	+ 
		     Dir['lib/**/*.rb'] 
  #s.require_path  = 'lib'
end
