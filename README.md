# ruby-earth


Original formulae and scripts from:

Latitude/longitude spherical geodesy formulae & scripts
(c) Chris Veness 2002-2015 
- www.movable-type.co.uk/scripts/latlong.html


This work was supported by the [Privamov](http://liris.cnrs.fr/privamov/project/) project funded by LABEX IMU (ANR-10-LABX-0088) of Université de Lyon, within the program "Investissements d’Avenir" (ANR-11- IDEX-0007) operated by the French National Research Agency (ANR)
