# coding: utf-8

require 'matrix'


# Allow calculation of distance on earth
#   $e = Earth::new
#   $e = Earth::new(model: Earth::WGS_84)
#   $e = Earth::new(model: { a: 6378137,   b: 6356752,  f: 1/298 })
#   $e = Earth::new(model: SPHERE)
#   $e = Earth::new(model: 6367449)
#
#   $e = Earth::new(mode: :rad)
#
#   $e = Earth::new( in: ->(p)   { [p[:x], p[:y]] },
#                   out: ->(x,y) { { x: x, y: y } })
#
#   $e.distance([longitude_1, latitude_1], [longitude_2, latitude_2])
#

class Earth
    VERSION = "0.1.0"
    # https://en.wikipedia.org/wiki/Earth_radius
    # https://en.wikipedia.org/wiki/Geographical_distance
    # https://en.wikipedia.org/wiki/Great-circle_distance
    # http://www.movable-type.co.uk/scripts/latlong.html
    # http://www.movable-type.co.uk/scripts/latlong-vincenty.html
    # http://williams.best.vwh.net/avform.htm
    # http://www.faqs.org/faqs/geography/infosystems-faq/
    
    # Convention:
    # x = λ = longitude
    # y = φ = latitude
    # 1     = from
    # 2     = to
   
    # https://en.wikipedia.org/wiki/Earth_ellipsoid

    # Ellipsoid IERS 2003
    IERS_2003   = { a: 6378136.6, b: 6356751.9,       f: 1/298.25642      }
    # Ellipsoid WGS-84 (Global GPS)
    WGS_84      = { a: 6378137,   b: 6356752.314245,  f: 1/298.257223563 }
    # Ellipsoid GRS-80 (Global ITRS)
    GRS_80      = { a: 6378137,   b: 6356752.314140,  f: 1/298.257222101 }
    # Ellipsoid FRS-67
    GRS_67      = { a: 6378160,   b: 6356774.719,     f: 1/298.247167    }
    # Ellipsoid Internation 1924
    INT_1924    = { a: 6378388,   b: 6356911.946,     f: 1/297           }
    # Ellipsoid Clarke 1880
    CLARKE_1880 = { a: 6378249.145, b: 6356514.86955, f: 1/293.465       }
    # Ellipsoid Airy
    AIRY_1830   = { a: 6377563.396, b: 6356256.909,   f: 1/299.3249646   }

    # Simple sphere
    SPHERE      = 6367449.145
    

    private
    @@deg2rad = Math::PI / 180
    @@rad2deg = 180 / Math::PI

    public
    # Errors
    class DivergingCalculus < StandardError ; end

    
    # Create new earth model
    # @param mode [Hash,Numeric]
    # @option opts :in   [Proc]
    # @option opts :out  [Proc]
    # @option opts :mode [:rad, :deg]
    def initialize(model: WGS_84, **opts, &block)
        case @model = model
        when Hash
            # Ellipsoid
            @a                     = @model[:a]
            @b                     = @model[:b]
            @f                     = @model[:f]

            # Radius
            # See: https://en.wikipedia.org/wiki/Earth_radius
            @e                     = Math.sqrt((@a*@a - @b*@b) / (@a*@a))
            @radius_mean           = (2*@a + @b)/3
            @radius_volumetric     = (@a*@a*@b) ** (1.0/3)
            @radius_authalic       = Math.sqrt(@a*@a/2 +
                                               @b*@b/2 * Math.atanh(@e)/@e)
            @radius_rectifying     = ((@a ** 1.5 + @b ** 1.5) / 2) ** (2.0/3)
            @radius_equatorial     = @a
            @radius_pole           = @b

            # Misc
            @meridional_mean       = @radius_rectifying

            # Default methods
            @distance              = method(:distance_vincenty)
            @destination           = method(:destination_vincenty)

        when Numeric
            # Ellipsoid
            @a                     = @model
            @b                     = @model
            @f                     = 0

            # Radius
            @radius_mean           = 
            @radius_volumetric     = 
            @radius_authalic       = 
            @radius_rectifying     = 
            @radius_equatorial     = 
            @radius_pole           = @model

            # Misc
            @meridional_mean       = @model

            # Default methods
            @distance              = method(:distance_haversine_formula)
            @destination           = method(:destination_sphere)
        end
        
        # Converter
        if block && opts.key?(:in)
            raise ArgumentError, "using named parameter :in AND block"
        end
        @in      = block || opts.delete(:in)
        @out     = opts.delete(:out)
        @mode    = opts.delete(:mode) || :deg
        
        if (@in  && !@in.respond_to?(:call)) ||
           (@out && !@out.respond_to?(:call))
            raise ArgumentError, ":converter doesn't respond to call method"
        end

        if ![ :deg, :rad ].include?(@mode)
            raise ArgumentError, ":mode must be :deg or :rad"
        end
    end

    attr_reader :radius_mean,       :radius_volumetric,
                :radius_authalic,   :radius_rectifying,
                :radius_equatorial, :radius_pole

    # Earth radius
    def radius
        @radius_rectifying
    end

    def initialBearing(from, to)
        λ1, φ1, λ2, φ2 = in_coords(from, to)
        θ              = _bearing(λ1, φ1, λ2, φ2)
        out_angle(θ, :brg)
    end

    def finalBearing(from, to)
        λ1, φ1, λ2, φ2 = in_coords(to, from)
        θ              = _bearing(λ1, φ1, λ2, φ2)
        out_angle(θ + Math::PI, :brg)
    end

    alias_method :bearing, :initialBearing

    # Distance from point p to great circle defined by (a,b)
    # @param p [Object] point
    # @param a [Object] point of line (a,b)
    # @param b [Object] point of line (a,b)
    # @return [Numeric] distance
    def great_circle_distance(p, a,b)
        cross_track_distance(p, a,b).abs
    end

    # Positive means right of the course from a to b, negative means left
    # @param p [Object] point
    # @param a [Object] point of line (a,b)
    # @param b [Object] point of line (a,b)
    # @return [Numeric] signed distance
    def cross_track_distance(p, a,b) 
        λ3, φ3, λ1, φ1, λ2, φ2 = in_coords(p, a,b)
        δ13 = _angular_distance(λ1, φ1, λ3, φ3)
        θ13 = _bearing(λ1, φ1, λ3, φ3)
        θ12 = _bearing(λ1, φ1, λ2, φ2)
        δxt = Math.asin(Math.sin(δ13) * Math.sin(θ13-θ12))
        radius * δxt
    end

    # Distance along the track.
    # ie: from a to x where x is the projection of p on (a,b).
    # @param p [Object] point
    # @param a [Object] point of line (a,b)
    # @param b [Object] point of line (a,b)
    # @return [Numeric] distance
    def along_track_distance(p, a,b)
        λ3, φ3, λ1, φ1, λ2, φ2 = in_coords(p, a,b)
        δ13 = _angular_distance(λ1, φ1, λ3, φ3)
        θ13 = _bearing(λ1, φ1, λ3, φ3)
        θ12 = _bearing(λ1, φ1, λ2, φ2)
        δxt  = Math.asin(Math.sin(δ13) * Math.sin(θ13-θ12))
        δat  = Math.acos(Math.cos(δ13) / Math.cos(δxt))
        radius * δat
    end
    
    # Crosstrack and along the track distances
    # @param p [Object] point
    # @param a [Object] point of line (a,b)
    # @param b [Object] point of line (a,b)
    # @return [Array<Numeric>] distance
    def track_distances(p, a,b)
        λ3, φ3, λ1, φ1, λ2, φ2 = in_coords(p, a,b)
        δ13 = _angular_distance(λ1, φ1, λ3, φ3)
        θ13 = _bearing(λ1, φ1, λ3, φ3)
        θ12 = _bearing(λ1, φ1, λ2, φ2)
        δxt  = Math.asin(Math.sin(δ13) * Math.sin(θ13-θ12))
        δat  = Math.acos(Math.cos(δ13) / Math.cos(δxt))
        [ radius * δxt, radius * δat ]
    end

    # Distance between two points on Earth
    # @param from [Object] start point
    # @param to   [Object] end point
    # @return [Numeric] distance
    def distance(from, to)
        @distance.call(from, to)
    end

    # Compute destination according to starting point, bearing and distance
    def destination(from, bearing, distance)
        @destination.call(from, bearing, distance)
    end
    
    # Compute centroid on a spherical surface
    #
    # @param data
    # @param type [:point, :line, :area]
    #        the type of centroid according to data.
    #        Only :point is supported for now
    # @return the coordinate of the centroid
    def centroid(data, type = :point)
        raise ArgumentError, "unsupported type" if type != :point
        
        v       = in_vectors(*data).reduce(:+)
        x, y, z = v.to_a
        
        λ = Math.atan2(y, x)
        φ = Math.atan2(z, Math.sqrt(x*x+y*y))
        out_coord(λ, φ)
    end


    # Compute midpoint
    # @param from [Object] start point
    # @param to   [Object] end point
    # @return     [Object] point
    def midpoint(from, to)
        λ1, φ1, λ2, φ2 = in_coords(from, to)

        Δλ = λ2-λ1
        bx = Math.cos(φ2) * Math.cos(Δλ);
        by = Math.cos(φ2) * Math.sin(Δλ);
        k  = Math.cos(φ1) + bx
        φ3 = Math.atan2(Math.sin(φ1) + Math.sin(φ2), Math.sqrt(k*k + by*by))
        λ3 = λ1 + Math.atan2(by, k);

        out_coord(λ3, φ3)
    end

    # Compute point on a fractional distance between from and to
    # @param from [Object] start point
    # @param to   [Object] end point
    # @param frac [Numeric]  0 <= frac <= 1
    # @return     [Object] point
    def fracpoint(from, to, frac)
        return midpoint(from, to) if frac == 0.5   # Midpoint shortcut

        if (frac < 0) || (frac > 1)
            raise ArgumentError, "frac argument must be between 0 and 1"
        end

        λ1, φ1, λ2, φ2 = in_coords(from, to)

        δ = _angular_distance(λ1, φ1, λ2, φ2)
        a = Math.sin((1-f) * δ) / Math.sin(δ)
        b = Math.sin(f     * δ) / Math.sin(δ)
        x = a * Math.cos(φ1) * Math.cos(λ1) + b * Math.cos(φ2) * Math.cos(λ2)
        y = a * Math.cos(φ1) * Math.sin(λ1) + b * Math.cos(φ2) * Math.sin(λ2)
        z = a * Math.sin(φ1)                + b * Math.sin(φ2)
        φi = atan2(z, Math.sqrt(x*x + y*y))
        λi = atan2(y, x)

        out_coord(λi, φi)
    end
    
    #
    # Destination
    #

    # (see #destination)
    def destination_vincenty(from, bearing, distance)
        λ1, φ1 = in_coord(from)
        α1     = in_angle(bearing)
        s      = distance
        λ2, φ2 = vincenty_direct(λ1, φ1, α1, s)[0..1]
        out_coord(λ2, φ2)
    rescue DivergingCalculus
        destination_sphere(from, bearing, distance)
    end

    
    # (see #destination)
    def destination_sphere(from, bearing, distance)
        λ1, φ1 = in_coord(from)
        α1     = in_angle(bearing)
        d      = distance / radius

        φ2 = Math.asin(Math.sin(φ1)*Math.cos(d) +
                       Math.cos(φ1)*Math.sin(d)*Math.cos(α1))
        Δλ = Math.atan2(Math.sin(α1)*Math.sin(d)*Math.cos(φ1),
                        Math.cos(d)-Math.sin(φ1)*Math.sin(φ2))
        λ2 = λ1 + Δλ

        # α2 = Math.atan2(Math.sin(-Δλ) * Math.cos(φ1), 
        #                 Math.cos(φ2)*Math.sin(φ1) -
        #                 Math.sin(φ2)*Math.cos(φ1)*Math.cos(-Δλ))

        out_coord(λ2, φ2) 
    end

    
    #
    # Distance
    #
        
    # Distance computed with the Vincenty inverse method
    # @note Will use the haversine formula as fallback if the
    #       vincenty method doesn't converge
    # @param from [Object] start point
    # @param to   [Object] end point
    # @return [Numeric] distance
    def distance_vincenty(from, to)
        λ1, φ1, λ2, φ2 = in_coords(from, to)
        vincenty_inverse(λ1, φ1, λ2, φ2)[0]
    rescue DivergingCalculus
        distance_haversine_formula(from, to)
    end

    # Spherical Earth projected to a plane
    # @note Only valid for distance of a few kilometers.
    #       At 20km, erros is between 9m-30m for 30°-70°
    # @param from [Object] start point
    # @param to   [Object] end point
    # @return [Numeric] distance
    def distance_flat_earth(from, to)
        return 0 if from == to
        λ1, φ1, λ2, φ2 = in_coords(from, to)

        # Pythagorean Theorem
        # Δλ = λ2 - λ1
        # Δφ = φ2 - φ1
	# φm = (φ1 + φ2) / 2
        # radius * Math.sqrt(Δφ**2 + (Math.cos(φm)*Δλ)**2)

        # Polar Coordinate Flat-Earth Formula (give smaller maximum errors)
        θ1 = Math::PI/2 - φ1
        θ2 = Math::PI/2 - φ2
        Δσ = sqrt(θ1*θ1 + θ2*θ2 - 2*θ1*θ2*Math.cos(λ2 - λ1))
        radius * Δσ
    end

    # Haversine formula
    # @note Haversine formula is the recommanded way to performe
    #       distance calculation on a spherical Earth
    # @param from [Object] start point
    # @param to   [Object] end point
    # @return [Numeric] distance
    def distance_haversine_formula(from, to)
        return 0 if from == to
        λ1, φ1, λ2, φ2 = in_coords(from, to)

        Δλ = λ2 - λ1
        Δφ = φ2 - φ1
        h  = Math.sin(Δφ/2)**2 + 
             Math.cos(φ1)*Math.cos(φ2)*Math.sin(Δλ/2)**2
        Δσ = 2 * Math.atan2(Math.sqrt(h), Math.sqrt(1-h))  
        radius * Δσ

        # If atan2 not available, 'min' can be used to protect against
        # rounding error, when computing Δσ as:
        # Δσ = 2 * Math.asin([1, Math.sqrt(h)].min) 
    end

    # Spherical law of cosines
    # @note Although this formula is mathematically exact, it is unreliable
    #       for small distances because the inverse cosine is ill-conditioned.
    #       Computation seems to be ok on 64bits float
    # @param from [Object] start point
    # @param to   [Object] end point
    # @return [Numeric] distance
    def distance_spherical_law_of_cosines(from, to)
        return 0 if from == to
        λ1, φ1, λ2, φ2 = in_coords(from, to)
        
        radius * Math.acos( 
            Math.sin(φ1) * Math.sin(φ2) + 
            Math.cos(φ1) * Math.cos(φ2) * Math.cos(λ2 - λ1))
    end

    # Chord length
    # @param from [Object] start point
    # @param to   [Object] end point
    # @return [Numeric] distance
    def distance_chord_length(from, to)
        return 0 if from == to
        λ1, φ1, λ2, φ2 = in_coords(from, to)

        Δx = Math.cos(φ2)*Math.cos(λ2) - Math.cos(φ1)*Math.cos(λ1)
        Δy = Math.cos(φ2)*Math.sin(λ2) - Math.cos(φ1)*Math.sin(λ1)
        Δz = Math.sin(φ2) - Math.sin(φ1)
        c  = Math.sqrt(Δx*Δx + Δy*Δy + Δz*Δz)
        Δσ = 2 * Math.asin(c/2)
        radius * Δσ
    end
        
    # N-vector
    # @param from [Object] start point
    # @param to   [Object] end point
    # @return [Numeric] distance
    def distance_n_vector(from, to, mode=:cross_dot)
        return 0 if from == to
        a,b = in_vectors(from, to)
        
	Δσ = case mode
	     when :dot       then Math.acos(a.dot(b))
	     when :cross     then Math.asin(a.cross(b).norm)
	     when :cross_dot then Math.atan2(a.cross(b).norm, a.dot(b))
             end
        radius * Δσ 
    end


    private

    def _bearing(λ1, φ1, λ2, φ2)
        Δλ = λ2 - λ1
        y  = Math.sin(Δλ) * Math.cos(φ2)
        x  = Math.cos(φ1) * Math.sin(φ2) -
             Math.sin(φ1) * Math.cos(φ2) * Math.cos(Δλ)
        θ  = Math.atan2(y, x)
    end

    # Angular distance using haversine formula
    def _angular_distance(λ1, φ1, λ2, φ2)
        Δλ = λ2 - λ1
        Δφ = φ2 - φ1
        h  = Math.sin(Δφ/2)**2 + 
             Math.cos(φ1)*Math.cos(φ2)*Math.sin(Δλ/2)**2
        Δσ = 2 * Math.atan2(Math.sqrt(h), Math.sqrt(1-h))  
    end

    #
    # Vincenty methods
    #

    # See: http://www.movable-type.co.uk/scripts/latlong-vincenty.html
    def vincenty_direct(λ1, φ1, α1, s)
        return [ λ1, φ1, α1 ] if s == 0

        sinα1  = Math.sin(α1)
        cosα1  = Math.cos(α1)
        tanU1  = (1-@f) * Math.tan(φ1)
        cosU1  = 1 / Math.sqrt((1 + tanU1*tanU1))
        sinU1 = tanU1 * cosU1
        σ1    = Math.atan2(tanU1, cosα1)
        sinα  = cosU1 * sinα1
        cos²α = 1 - sinα*sinα
        u²    = cos²α * (@a*@a - @b*@b) / (@b*@b);
        _A    = 1 + u²/16384*(4096+u²*(-768+u²*(320-175*u²)))
        _B    = u²/1024 * (256+u²*(-128+u²*(74-47*u²)))

        cos2σm, sinσ, cosσ, Δσ = []
        
        σ, σʹ, iterations = s / (@b*_A), 0, 0
        loop do 
            cos2σm = Math.cos(2*σ1 + σ);
            sinσ   = Math.sin(σ);
            cosσ   = Math.cos(σ);
            Δσ     = _B*sinσ*(cos2σm+_B/4*(cosσ*(-1+2*cos2σm*cos2σm)-
                           _B/6*cos2σm*(-3+4*sinσ*sinσ)*(-3+4*cos2σm*cos2σm)))
            σʹ     = σ;
            σ      = s / (@b*_A) + Δσ;
            raise DivergingCalculus if (iterations += 1) >= 200
            break                   if (σ-σʹ).abs        <= 1e-12
        end

        x  = sinU1*sinσ - cosU1*cosσ*cosα1
        φ2 = Math.atan2(sinU1*cosσ + cosU1*sinσ*cosα1,
                        (1-@f)*Math.sqrt(sinα*sinα + x*x))
        λ  = Math.atan2(sinσ*sinα1, cosU1*cosσ - sinU1*sinσ*cosα1)
        _C = @f/16*cos²α*(4+@f*(4-3*cos²α))
        _L = λ - (1-_C) * @f * sinα *
                 (σ + _C*sinσ*(cos2σm+_C*cosσ*(-1+2*cos2σm*cos2σm)))
        λ2 = λ1 + _L 
        α2 = Math.atan2(sinα, -x)

        [ λ2, φ2, α2 ]
    end

    # See: http://www.movable-type.co.uk/scripts/latlong-vincenty.html
    def vincenty_inverse(λ1, φ1, λ2, φ2)
        return [0, Float::NAN, Float::NAN] if (λ1 == λ2) && (φ1 == φ2)
            
        l       = λ2 - λ1
        
        tanU1   = (1-@f) * Math.tan(φ1)
        cosU1   = 1 / Math.sqrt((1 + tanU1**2))
        sinU1   = tanU1 * cosU1
        
        tanU2   = (1-@f) * Math.tan(φ2)
        cosU2   = 1 / Math.sqrt((1 + tanU2**2))
        sinU2   = tanU2 * cosU2
        
        σ, sinλ, cosλ, cos²α, sinσ,cos2σm,cosσ = []
        
        λ, λʹ, iterations = l,0,0
        loop do 
            sinλ   = Math.sin(λ)
            cosλ   = Math.cos(λ)
            sinσ   = Math.sqrt((cosU2*sinλ)**2 +
                               (cosU1*sinU2-sinU1*cosU2*cosλ)**2)
            cosσ   = sinU1*sinU2 + cosU1*cosU2*cosλ
            σ      = Math.atan2(sinσ, cosσ)
            sinα   = cosU1 * cosU2 * sinλ / sinσ
            cos²α = 1 - sinα*sinα
            cos2σm = cosσ - 2*sinU1*sinU2/cos²α
            cos2σm = 0 if cos2σm.nan?   # equatorial line
            c      = @f/16*cos²α*(4+@f*(4-3*cos²α))
            λʹ     = λ
            λ      = l + (1-c) * @f * sinα *
                             (σ + c*sinσ*(cos2σm+c*cosσ*(-1+2*cos2σm*cos2σm)))
            raise DivergingCalculus if (iterations += 1) >= 200
            break                   if (λ-λʹ).abs        <= 1e-12
        end

        u² = cos²α * (@a*@a - @b*@b) / (@b*@b)
        _A = 1 + u²/16384*(4096+u²*(-768+u²*(320-175*u²)))
        _B = u²/1024 * (256+u²*(-128+u²*(74-47*u²)))
        Δσ = _B*sinσ*(cos2σm+_B/4*(cosσ*(-1+2*cos2σm*cos2σm)-
                           _B/6*cos2σm*(-3+4*sinσ*sinσ)*(-3+4*cos2σm*cos2σm)))

        s  = @b*_A*(σ-Δσ)
        α1 = Math.atan2(cosU2*sinλ,  cosU1*sinU2-sinU1*cosU2*cosλ)
        α2 = Math.atan2(cosU1*sinλ, -sinU1*cosU2+cosU1*sinU2*cosλ)
        
        [ s.round(3), α1, α2 ]
    end

    
    #
    # Converters
    #

    def out_coord(λ, φ)
        p = [ out_angle(λ, :lon), out_angle(φ, :lat) ]
        @out ? @out.call(*p) : p
    end
    
    def out_angle(α, mode = :pos)
        if @mode == :deg
            # /!\ If normalizing latitude, it will change longitude
            case mode
            when :lat             then α*@@rad2deg
            when :neg, :lon, :brg then (α*@@rad2deg + 540) % 360 - 180
            when :pos             then (α*@@rad2deg + 360) % 360
            else raise ArgumentError, "invalid mode"
            end
        else
            α
        end
    end

    def in_angle(α)
        @mode == :deg ? α*@@deg2rad : α
    end
    
    def in_coord(p)
        lng, lat = @in ? @in.call(p) : p
        if @mode == :deg
        then [ λ = lng*@@deg2rad, φ = lat*@@deg2rad ]   # x = λ = longitude
        else [ λ = lng,           φ = lat           ]   # y = φ = latitude
        end
    end

    def in_vector(p)
        λ, φ = in_coord(p)
	Vector[ Math.cos(φ)*Math.cos(λ),
	        Math.cos(φ)*Math.sin(λ),
	        Math.sin(φ) ]
    end

    def in_coords(*args)
        args.flat_map {|p| in_coord(p) }
    end
    
    def in_vectors(*args)
        args.map {|p| in_vector(p) }
    end
end


